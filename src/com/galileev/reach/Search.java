package com.galileev.reach;

import java.io.PrintStream;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class Search {
	private Queue<Path> paths;
	private Set<Method> visited;
	private Matcher matcher;
	private List<SpecialMethodRule> methodRules;

	public Search(Class<?> start, Matcher matcher, List<SpecialMethodRule> methodRules) {
		paths = new LinkedList<Path>();
		paths.add(new Path(null, null, start));

		visited = new HashSet<Method>();

		this.matcher = matcher;
		this.methodRules = methodRules;
	}

	public void run() {
		while (!paths.isEmpty()) {
			Path path = paths.poll();
			try {
				for (Method m : path.clazz.getMethods())
					processMethod(m, getReturnType(m), path);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void processMethod(Method method, Type type, Path parent) {
		if (!visited.add(method))
			return;

		for (Class<?> c : collectClasses(type)) {
			Path path = new Path(parent, method, c);
			if (matcher.isMatch(c, method)) {
				path.print(System.out);
				System.out.println();
			} else
				paths.add(path);
		}
	}

	private Type getReturnType(Method method) {
		if (methodRules != null) {
			for (SpecialMethodRule rule : methodRules)
				if (rule.matches(method))
					return rule.getReturnType();
		}
		return method.getGenericReturnType();
	}

	private static Set<Class<?>> collectClasses(final Type type) {
		return resolveType(type, new HashSet<Class<?>>(), new HashSet<Type>());
	}

	private static Set<Class<?>> resolveType(final Type type, final Set<Class<?>> result, final Set<Type> visited) {
		if (!visited.add(type)) return result;

		if (type instanceof Class<?>)
			result.add((Class<?>) type);
		else if (type instanceof TypeVariable)
			for (Type t : ((TypeVariable<?>) type).getBounds())
				resolveType(t, result, visited);
		else if (type instanceof GenericArrayType)
			resolveType(((GenericArrayType) type).getGenericComponentType(), result, visited);
		else if (type instanceof ParameterizedType) {
			ParameterizedType pt = (ParameterizedType) type;
			
			resolveType(pt.getRawType(), result, visited);
				
			for (Type t : pt.getActualTypeArguments())
				resolveType(t, result, visited);
		} 
		
		return result;
	}

	private static class Path {
		Path parent;
		Method method;
		Class<?> clazz;

		public Path(Path parent, Method method, Class<?> target) {
			this.parent = parent;
			this.method = method;
			this.clazz = target;
		}

		public void print(PrintStream stream) {
			if (parent != null)
				parent.print(stream);

			if (method != null) {
				stream.print(".");
				stream.print(method.getName());
			} else {
				stream.print(clazz.getCanonicalName());
			}
		}
	}
}
