package com.galileev.reach;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.regex.Pattern;

public interface Matcher {
	boolean isMatch(Type type, Method m);

	public static class ClassMatcher implements Matcher {
		private final Class<?> clazz;
	
		public ClassMatcher(final Class<?> c) {
			clazz = c;
		}
	
		@Override
		public boolean isMatch(final Type type, final Method m) {
			return clazz.equals(type);
		}
	
	}

	public static class MethodNameMatcher implements Matcher {
		private final Pattern pattern;
	
		public MethodNameMatcher(final String regex) {
			this(Pattern.compile(regex));
		}
	
		public MethodNameMatcher(Pattern pattern) {
			this.pattern = pattern;
		}
	
		@Override
		public boolean isMatch(final Type type, final Method m) {
			return pattern.matcher(m.getName()).find();
		}
	}
	
	public static class AndMatcher implements Matcher {
		private Matcher[] matchers;

		public AndMatcher(Matcher... matchers) {
			this.matchers = matchers;
		}

		@Override
		public boolean isMatch(Type type, Method method) {
			for (Matcher m : matchers)
				if (!m.isMatch(type, method)) return false;
					
			return true;
		}
	}
}