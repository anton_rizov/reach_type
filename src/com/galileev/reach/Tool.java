package com.galileev.reach;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class Tool {
	public static void main(String[] args) throws Exception {
		if (args.length < 3) {
			usage(System.out);
			return;
		}
		
		Class<?> start = Class.forName(args[0]);
		Matcher matcher = null;
		List<SpecialMethodRule> rules = null;
		for (int i = 2; i < args.length; i += 2) {
			String option = args[i - 1];
			String val = args[i];
			if (option.equals("-n") || option.equals("-method")) {
				matcher = and(matcher, new Matcher.MethodNameMatcher(val));
			} else if (option.equals("-t") || option.equals("-return")) {
				matcher = and(matcher, new Matcher.ClassMatcher(findClass(val)));
			} else if (option.equals("-rc")) {
				rules = loadRules(new File(val));
			}
		}

		new Search(start, matcher, rules).run();
	}

	private static void usage(PrintStream stream) {
		stream.println("reach start_class [-rc file] {-n method name|-t class name}");
	}

	private static Matcher and(Matcher a, Matcher b) {
		return a == null ? b : new Matcher.AndMatcher(a, b);
	}
	
	private static List<SpecialMethodRule> loadRules(File file) throws IOException {
		List<SpecialMethodRule>rules = new ArrayList<SpecialMethodRule>();

		BufferedReader r = null;
		try {
			r = new BufferedReader(new FileReader(file));
			String line = null;
			while ((line = r.readLine()) != null) {
				if (line.startsWith("#")) continue;

				line = line.trim();
				int x = line.indexOf(':');
				String method = line.substring(0, x).trim();
				try {
					Class<?> c = findClass(line.substring(x + 1).trim());

					rules.add(new SpecialMethodRule( method, c ));
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		} finally {
			if (r != null)	r.close();
		}

		return rules;
	}
	
	private static Class<?> findClass(String name) throws ClassNotFoundException {
		if (name.equals("byte")) return byte.class;
		if (name.equals("short")) return short.class;
		if (name.equals("int")) return int.class;
		if (name.equals("long")) return long.class;
		if (name.equals("char")) return char.class;
		if (name.equals("float")) return float.class;
		if (name.equals("double")) return double.class;
		if (name.equals("boolean")) return boolean.class;
		if (name.equals("void")) return void.class;
		
		return Class.forName(name);
	}
}
