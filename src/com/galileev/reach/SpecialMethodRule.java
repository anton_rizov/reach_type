package com.galileev.reach;

import java.lang.reflect.Method;

public class SpecialMethodRule {
	String methodName;
	Class<?> clazz;

	public SpecialMethodRule(String methodName, Class<?> clazz) {
		super();
		this.methodName = methodName;
		this.clazz = clazz;
	}

	public boolean matches(Method m) {
		return matches(m.getName());
	}
	
	public boolean matches(String actualName) {
		return actualName.contains(methodName);
	}

	public Class<?> getReturnType() {
		return clazz;
	}
}
